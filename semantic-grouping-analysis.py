# Created by joecool890
# Data organization in python using pandas
# Version 0.2.3
# customized for semantic-grouping (aka apples and oranges)

import numpy as np
import glob, os, time
import pandas as pd

# If on windows, change directory
if  os.name == "nt":
    os.chdir("Insert directory here that fits your root folder. After you do this, ./ will work!")

data_dir    = "./data/*.csv"
surv_dir    = "./data/*.txt"

# Extract all data files into single np array
data_files      = glob.glob(data_dir)
surv_files      = glob.glob(surv_dir)
participants    = len(data_files)

# Data Thresholding
RT_thresholding = 1 # 0 if no thresholding
RT_min      = 150
RT_max      = 1500
acc_thresh  = 90

x = time.time()
# Load all data files into single panda data frame
raw_data = []
for file in range(participants):
    data = pd.read_csv(data_files[file], index_col = "parNo", header = 0)
    raw_data.append(data)

# all data into a single data frame
data_frame = pd.concat(raw_data)

# Grab accuracy for participants
overall_acc     = data_frame.groupby(["parNo"])["corr"].mean()*100
condition_acc   = data_frame.groupby(["parNo","Condition"])["corr"].mean().unstack(["Condition"])*100
# overall_acc = data_frame.groupby(["parNo","Condition","tar","Category"])["corr"].mean().unstack(["Condition", "tar","Category"])*100

frames = [overall_acc, condition_acc]
all_results_acc = pd.concat(frames, axis = 1)
all_results_acc.columns = ["all_acc", "1_acc","2_acc","3_acc","4_acc","5_acc","6_acc"]

# Number of trials that are correct
corr_data       = data_frame[(data_frame["corr"] == 1)]
corr_data_count = corr_data.groupby(["parNo"])["RT"].count()

# Grab only correct data for RT
if RT_thresholding == 1:
    corr_data = corr_data[(corr_data["RT"] > RT_min) & (corr_data["RT"] < RT_max)]
excluded_data_count = [corr_data_count - corr_data.groupby(["parNo"])["RT"].count(),  (corr_data_count - corr_data.groupby(["parNo"])["RT"].count())/corr_data_count*100]

# concat trial count + % into one
excluded_data_count         = pd.concat(excluded_data_count,axis = 1)
excluded_data_count.columns = ["Dropped Trials", "Drop Rate"]

# Create headers for RT data
analysis    = ["mean","median","std", "inverseEfficiency"]
conditions  = sorted(data_frame.Condition.unique())
RT_labels = []
for labels2 in range(len(analysis)):
    for labels in conditions:
        A = "".join([str(conditions[labels-1]), "_", str(analysis[labels2])])
        RT_labels.append(A)

# RT by conditions
condition_RT        = corr_data.groupby(["parNo","Condition"])["RT"].agg(analysis[0:3]).unstack(["Condition"])
inverse_efficiency  = condition_RT.iloc[:,0:6]/condition_acc*100
all_RT              = pd.concat([condition_RT, inverse_efficiency], axis = 1)
all_RT.columns      = RT_labels

# concat all data into single data frame, update when necessary
all_data            = pd.concat([excluded_data_count, all_results_acc, all_RT],axis = 1)
# exclude data that is less than accuracy threshold
all_data = all_data[all_data.all_acc > acc_thresh]
all_data.to_csv("semantic-grouping_no-reference.csv")

time_elapsed = time.time() - x
print("success! time elapsed: " + str(round(time_elapsed,5)*1000) + " ms")

# do not delete - note to self
# overall_RT = corr_data.groupby(["parNo","Condition","tar"])["RT"].mean().unstack(["Condition","tar"])
# overall_RT.columns = ['1_horz','1_vert','2_horz','2_vert','3_horz','3_vert','4_horz','4_vert','5_horz','5_vert','6_horz','7_vert']
# all_data.to_csv("all_data.csv")
# print(overall_RT)
# save pandas dataframe into numpy array
# np_overall_RT = overall_RT.valuedata